# Vivian WebGL Test Project 
 
## How to Run 

1. Checkout the master.
+ If you use a GUI client, like GitKraken, submodules should be cheked out when you checkout the branch.
+ This is not automatic if you use git bash. To directly clone the branch with submodules checked out:  
    + `git clone --recurse-submodules https://gitlab.gwdg.de/usability-engineering-ar-mr-vr/vivian-framework/vivian-webgl-test-project.git`
    + [More info on submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)
2. Open the project in Unity.
3. In the Build Settings, set the Platform to WebGL.
4. Select the Scene you want to build, e.g. VivianWebGLTestSceneCoffeeMachine.
5. Hit "Build And Run".
6. To run the build on a server, simply copy the created destination folder to a webserver.

## Batch Build

You can also build a scene on command line. For this open a cmd in the root folder, and type `build.bat <prototypeName> <bundleURL>`.
The prototype name can be `coffeemachine`, `microwave`, etc... The bundle URL should be the path where the WebGL application will look for the prototype. This can either be an included prototype, e.g. `Microwave` or a remote URL, as in the following:
   + `build.bat microwave https://my.server.de/WebGL/microwave/microwave`
+ The `build.bat` script builds a WebGL app in the folder `Builds/WebGL` of the project. You can either upload the WebGL folder on a server or run it on a local server.
+ Note that the script uses the default `Unity 2019.1.10f1` installation path (`C:\Program Files\Unity\Hub\Editor\2019.1.10f1`). If that is not the case for you, you can edit the script.
