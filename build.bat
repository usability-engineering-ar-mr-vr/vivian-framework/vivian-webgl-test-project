@ECHO OFF

if [%1]==[] goto usage

ECHO Attempting to build a WebGL app...
"C:\Program Files\Unity\Hub\Editor\2019.1.10f1\Editor\Unity.exe" -logFile "%~dp0Builds\build.log" -quit -batchmode -projectPath "%~dp0" -buildTarget WebGL -executeMethod AutoBuild.PerformWebGLBuild %1 %2 

for %%i in ("%1") do set "prefabName=%%~nxi"

ECHO Copying the %prefabName% AssetBundle to the build folder...
XCOPY Assets\AssetBundles\WebGL\%prefabName% Builds\WebGL\%prefabName%\%prefabName%
ECHO Done!

PAUSE
goto :eof

:usage
ECHO Usage: build.bat ^<bundleURL^> ^[pefabName^] 



