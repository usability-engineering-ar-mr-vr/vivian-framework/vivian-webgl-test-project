﻿using System;
using System.Linq;
using de.ugoe.cs.vivian.core;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
public class AutoBuild : MonoBehaviour
{
    static void PerformWebGLBuild()
    {
        //This method can be called from a windows cmd
        //Builds a complete WebGL application
        
        //Arguments passed from the cmd, Calling should look like
        //'path\to\Unity.exe -logFile path\to\project\Builds\build.log -quit -batchmode -projectPath path\to\project -buildTarget WebGL -executeMethod AutoBuild.PerformWebGLBuild bundleURL"
        string[] arguments = Environment.GetCommandLineArgs();
        String bundleURL = arguments[11];
        String prefabName = arguments.Length == 13 ? arguments[12] : bundleURL.Split(new[] {'/'}, StringSplitOptions.RemoveEmptyEntries).Last();
        
        //Opens the test scene
        string[] scenes = {"Assets/Scenes/VivianWebGLTestScene.unity"};
        EditorSceneManager.OpenScene(scenes[0]);

        //Gets the VivianFramework prefab
        VirtualPrototype vivianPrefab = GameObject.Find("VivianFramework").GetComponent<VirtualPrototype>();
        //Sets the URL and name of the prototype from arguments passed in cmd
        vivianPrefab.BundleURL = bundleURL;
        vivianPrefab.PrototypePrefabName = prefabName;

        //Saves the scene to keep the prefab with the good values
        Undo.RecordObject(vivianPrefab, "Updated prototype name and bundle URL");
        PrefabUtility.RecordPrefabInstancePropertyModifications(vivianPrefab);

        //Builds AssetBundles
        CreateAssetBundles.BuildAllAssetBundles();
       
        //Builds WebGL app in the Builds folder of the project
        BuildPipeline.BuildPlayer(scenes, "Builds/WebGL/" + bundleURL.Split(new[] {'/'}, 
                StringSplitOptions.RemoveEmptyEntries).Last(), BuildTarget.WebGL, BuildOptions.CompressWithLz4);
    }
}
#endif
